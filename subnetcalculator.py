##def main():
##    # Get IP and netmask
##    ip = ask_for_number_sequence("Wat is het IP-adres?")
##    netmask = ask_for_number_sequence("Wat is subnetmask?")
## 
##    print("ip is: ",ip, "masker is:",netmask)
## 
##    # Check ip-address
##    isValidIp = is_valid_ip_address(ip)
## 
##    # Check netmask
##    isValidNetMask = is_valid_netmask(netmask)
## 
##    #one_bits_in_netmask()
## 
##def ask_for_number_sequence(message):
##  #input IP and netmask
##  ip  = []
##  print(message)
##  ipLine = input()
##  ip = ipLine.split('.')
##  ip= [int(i) for i in ip]
##
##  netmask  = []
##  netmaskLine = input()
##  netmask = netmaskLine.split('.')
##  netmask= [int(i) for i in netmask]
##  return ip, netmask
## 
##def is_valid_ip_address(ip):
##    #control IP
##    validIp= True
##    ipcount = 0
##    for i in ip:
##      if i < 0 or i> 255:
##        validIp=False
##      else:
##        ipcount +=1
## 
##    if ipcount!=4:
##      validIp=False
## 
##    # Return
##    return validIp


#old code
import random
import sys

def main():
  message=ask_for_number_sequence()
  IP, netmask=message
  #print(IP, netmask)
  validip=is_valid_ip_address(IP)
  #print("validip is ",validip)
  netmaskdata=is_valid_netmask(netmask)
  validmask, binary_netmask= netmaskdata
  #print("validmask is ",validmask)
  #print(binary_netmask)
  bitcount=one_bits_in_netmask(binary_netmask)
  network_decimal=apply_network_mask(binary_netmask,IP)
  wildcard_mask=netmask_to_wildcard_mask(netmask)
  broadcast_decimal=get_broadcast_address(binary_netmask, IP)
  no_hosts=prefix_length_to_max_hosts(binary_netmask, IP)
  if validmask is True and validip is True:
    print("IP-adres en subnetmasker zijn geldig.")
  else:
    print("IP-adres en/of subnetmasker is ongeldig")
  print ("De lengte van het subnetmasker is ",bitcount)
  print ("Het adres van het subnet is ", network_decimal)
  print("Het wildcardmasker is ",wildcard_mask)
  print ("Het broadcastadres is ", broadcast_decimal)
  print("Het maximaal aantal hosts op dit subnet is ", no_hosts)

    
  

  
def ask_for_number_sequence():
  #input IP and netmask
  IPline= input("Wat is het IP-adres?")
  IP=IPline.split('.')
  IP= [int(i) for i in IP]
  
  netmask=[]
  netmaskline= input("Wat is het subnetmasker?")
  netmask=netmaskline.split('.')
  netmask= [int(i) for i in netmask]

  return IP, netmask




def is_valid_ip_address(IP):
    #control IP
  
  validip= True
  ipcount = 0
  for i in IP:
    if i < 0 or i> 255:
      
      validip=False
    else:
      ipcount +=1

  if ipcount!=4:
    validip=False

  return validip




def is_valid_netmask(netmask):
  #control netmask
  
  binary=[]
  validmask= True
  maskcount = 0

  for i in netmask:
    if i < 0 or i> 255:
      
      validmask=False
    else:
      maskcount +=1

  if maskcount!=4:
    validmask=False 


  for i in netmask:
    binary.append(f"{i:08b}")
  binary_netmask= ''.join(binary)

  
  checking_ones=True

  for i in binary_netmask:
    if i=="0":
      checking_ones=False
    elif i=="1" and checking_ones==False:
      validmask=False

  return validmask,binary_netmask





def one_bits_in_netmask(binary_netmask):
    #count 1 bits
  bitcount=0
  #binary2=[]
  
  #for i in netmask:
    #binary2.append(f"{i:08b}")
  #binary_netmask2= ''.join(binary2)
  #print (binary_netmask2)
  #binary_netmask2 = list(binary_netmask2)
  #print (binary_netmask2)
  for i in binary_netmask:
    if i=="1":
      bitcount+=1



  return bitcount




def apply_network_mask(binary_netmask,IP):
#address van het subnet berekenen, no clue
  #calculate hosts

  binary_ip = []

  bin_ip_oct = [bin(i).split("b")[1] for i in IP]

  for i in range(0,len(bin_ip_oct)):
    if len(bin_ip_oct[i]) < 8:
      padded_bin = bin_ip_oct[i].zfill(8)
      binary_ip.append(padded_bin)
    else:
      binary_ip.append(bin_ip_oct[i])

  

  ip_bin_mask = "".join(binary_ip)

  
  no_zeros = binary_netmask.count("0")
  no_ones = 32 - no_zeros
  no_hosts = abs(2 ** no_zeros - 2)

  binary_network = ip_bin_mask[:no_ones] + "0" * no_zeros
  binary_network_oct=[]
  binary_network_oct = [binary_network[i * 8:i * 8+8] for i,blah in enumerate(binary_network[::8])]
  binary_network_oct = [int(i) for i in binary_network_oct]

  binary_calc=[]
  for binary in binary_network_oct:
    binary1 = binary 
    decimal, i, n = 0, 0, 0
    while(binary != 0): 
        dec = binary % 10
        decimal = decimal + dec * pow(2, i) 
        binary = binary//10
        i += 1
    binary_calc.append(decimal)
    

  binary_calc=[ str(i) for i in binary_calc]
  network_decimal = ".".join(binary_calc)




  return network_decimal

  
  
def netmask_to_wildcard_mask(netmask):
  wild_mask = []
  for i in netmask:
    wild_bit = 255 - i
    wild_mask.append(wild_bit)
  wildcard_mask = ".".join([str(i) for i in wild_mask])

  return wildcard_mask




def get_broadcast_address(binary_netmask, IP):

  binary_ip = []

  bin_ip_oct = [bin(i).split("b")[1] for i in IP]

  for i in range(0,len(bin_ip_oct)):
    if len(bin_ip_oct[i]) < 8:
      padded_bin = bin_ip_oct[i].zfill(8)
      binary_ip.append(padded_bin)
    else:
      binary_ip.append(bin_ip_oct[i])

  

  ip_bin_mask = "".join(binary_ip)

  
  no_zeros = binary_netmask.count("0")
  no_ones = 32 - no_zeros
  no_hosts = abs(2 ** no_zeros - 2)

  binary_broadcast = ip_bin_mask[:no_ones] + "1" * no_zeros
  binary_broadcast_oct=[]
  binary_broadcast_oct = [binary_broadcast[i * 8:i * 8+8] for i,blah in enumerate(binary_broadcast[::8])]
  binary_broadcast_oct = [int(i) for i in binary_broadcast_oct]
  
  binary_calc2=[]
  for binary0 in binary_broadcast_oct:
    binary2 = binary0 
    decimal2, i, n = 0, 0, 0
    while(binary0 != 0): 
        dec1 = binary0 % 10
        decimal2 = decimal2 + dec1 * pow(2, i) 
        binary0 = binary0//10
        i += 1
    binary_calc2.append(decimal2)

  binary_calc2=[ str(i) for i in binary_calc2]
  broadcast_decimal = ".".join(binary_calc2)
  


  return broadcast_decimal




def prefix_length_to_max_hosts(binary_netmask, IP):

  binary_ip = []

  bin_ip_oct = [bin(i).split("b")[1] for i in IP]

  for i in range(0,len(bin_ip_oct)):
    if len(bin_ip_oct[i]) < 8:
      padded_bin = bin_ip_oct[i].zfill(8)
      binary_ip.append(padded_bin)
    else:
      binary_ip.append(bin_ip_oct[i])

  

  ip_bin_mask = "".join(binary_ip)

  
  no_zeros = binary_netmask.count("0")
  no_ones = 32 - no_zeros
  no_hosts = abs(2 ** no_zeros - 2)

  return no_hosts

  

if __name__ == "__main__":
    main()



#ask_for_number_sequence()
  
#is_valid_ip_address()

#is_valid_netmask()

#one_bits_in_netmask()
