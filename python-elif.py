byte1 = 8
byte2 = 136
byte3 = 120
byte4 = 160

if byte1 > 255 or byte2 > 255 or byte3 > 255 or byte4 > 255:
  print ("de vier bytes vormen geen geldig IPv4-adres")
elif byte1 <= 10 and byte2 <= 255 and byte3 <= 255 and byte4 <= 255:
  print ("de vier bytes vormen een IPv4-adres in het bereik 10.0.0.0/8")
else:
  print ("de vier bytes vormen een IPv4-adres buiten het bereik 10.0.0.0/8")
