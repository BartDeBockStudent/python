import subprocess

IP = '8.8.8.8'
ping_command = "ping -n 2 -w 4 " + IP

(output, error) = subprocess.Popen(ping_command,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True).communicate()

print(output, error)
